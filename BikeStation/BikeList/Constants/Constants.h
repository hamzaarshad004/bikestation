//
//  Constants.h
//  BikeList
//
//  Created by ladmin on 02/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

// API URLs
#define STATION_LIST_API @"https://feeds.citibikenyc.com/stations/stations.json"

// CoreData Entity Name
#define BIKE_LIST @"BikeList"

// Station Attribute Keys
#define IDENTITY @"identity"
#define STATION_NAME @"stationName"
#define STATUS_VALUE @"statusValue"
#define STATUS_KEY @"statusKey"
#define AVAILABLE_DOCKS @"availableDocks"
#define STATIONS_LIST_NAME @"stationBeanList"

// Station Attribute Value
#define STATUS_VALUE_IN_SERVICE @"In Service"
#define STATUS_VALUE_OFF_SERVICE @"Not In Service"

// NSURLSession
#define ACCEPT_ALL_HEADERS @"Accept" : @"*/*"

#endif /* Constants_h */
