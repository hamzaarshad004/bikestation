//
//  BikeCollectionCollectionViewController.h
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface BikeCollectionViewController : UICollectionViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (strong, nonatomic) IBOutlet UICollectionView *colView;
@property (strong, nonatomic) IBOutlet UINavigationItem *navigationItem;
- (IBAction)deleteBikeStation:(id)sender;
- (IBAction)refreshCoreData:(id)sender;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loader;


-(void) getBikeList;
-(void) getBikeFromAPI:(void (^)(void))completionBlock;
-(void) populateCoreData;
-(void) loadingState: (int) switchState;

@end

NS_ASSUME_NONNULL_END
