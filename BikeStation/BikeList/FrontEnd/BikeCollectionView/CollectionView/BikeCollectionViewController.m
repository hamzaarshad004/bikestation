//
//  BikeCollectionCollectionViewController.m
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import "BikeCollectionViewController.h"
#import "CollectionViewHeader.h"
#import "StationCollectionViewCell.h"
#import "DetailTableViewController.h"
#import "Station.h"
#import <dispatch/dispatch.h>
#import <CoreData/CoreData.h>
#import "Constants.h"
#import "APIManager.h"

#import "MBProgressHUD.h"

@interface BikeCollectionViewController (){
    __block NSDictionary *json;
    __block NSArray *stations;
    __block NSManagedObjectContext *context;
}

@property (strong) NSMutableArray *bikes;

@end

@implementation BikeCollectionViewController

static NSString * const reuseIdentifier = @"Station";
@synthesize colView;

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewWillAppear:(BOOL) animated];
    [self.loader setHidden:YES];
    [self loadingState: 1];
    [self getBikeList];
}


- (IBAction)deleteBikeStation:(id)sender {
    
    StationCollectionViewCell *cell = (StationCollectionViewCell *)[[sender superview] superview];
    NSIndexPath *indexPath = [self.colView indexPathForCell:cell];

    CoreDataManager *coreDataManager = [CoreDataManager getCoreDataManager];
    [coreDataManager deleteStation:[self.bikes objectAtIndex:indexPath.row]];
    
    [self getBikeList];
}

- (IBAction)refreshCoreData:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self loadingState:1];
    });
    [self populateCoreData];
}

- (void) getBikeList{
    [self.bikes removeAllObjects];
    
    CoreDataManager *coreDataManager = [CoreDataManager getCoreDataManager];
    self.bikes = [coreDataManager getStationList];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [self->colView reloadData];
        
        [self loadingState: 0];
    });
}

-(void) getBikeFromAPI:(void (^)(void))completionBlock{
    
    APIManager *apiManager = [APIManager getAPIManager];
    [apiManager getBikeStations:^(NSDictionary* data){
        self->json = data;
        self-> stations = self->json[STATIONS_LIST_NAME];
        if (completionBlock){
            completionBlock();
        }
    }];
}

-(void) populateCoreData{
    
    [self getBikeFromAPI:^{
        
        CoreDataManager *coreDataManager = [CoreDataManager getCoreDataManager];
        [coreDataManager populateStationList:self->stations];
        
        [self getBikeList];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            [self loadingState:0];
        });
    }];
    
}

#pragma mark Loading State Function

-(void) loadingState: (int) switchState{
    if (switchState == 1){
        self.refreshButton.enabled = NO;
        [self.colView setAlpha:0.5];
        self.colView.scrollEnabled = NO;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    else if (switchState == 0){
        self.refreshButton.enabled = YES;
        [self.colView setAlpha:1.0];
        self.colView.scrollEnabled = YES;
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"showStationDetail"]){
        NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
        DetailTableViewController *destViewController = segue.destinationViewController;
        NSIndexPath *indexPath = [indexPaths objectAtIndex:0];
        destViewController.bike = [self.bikes objectAtIndex:indexPath.row];
        destViewController.index = indexPath.row;
        [self.collectionView deselectItemAtIndexPath:indexPath animated:NO];
    }
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.bikes count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    StationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    if ([self.bikes count] != 0){
        [cell setTableCells: [self.bikes objectAtIndex: indexPath.row]];
    }
    
    return cell;
} 

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewHeader *headerView = nil;
    if (kind == UICollectionElementKindSectionHeader){
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier: @"HeaderView" forIndexPath:indexPath];
        headerView.titleLabel.text = @"Stations and their Count";
    }
    return headerView;
}


#pragma mark <UICollectionViewDelegate>


@end
