//
//  StationCollectionViewCell.h
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface StationCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblStationName;
@property (strong, nonatomic) IBOutlet UILabel *lblAvailableDocks;
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

-(void) setTableCells: (NSManagedObject*) station;

@end

NS_ASSUME_NONNULL_END
