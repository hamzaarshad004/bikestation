//
//  StationCollectionViewCell.m
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import "StationCollectionViewCell.h"
#import "Constants.h"

@implementation StationCollectionViewCell

-(void) setTableCells: (NSManagedObject*) station{
    if (station){
        self.lblStationName.text = [station valueForKey:STATION_NAME];
        self.lblAvailableDocks.text = [NSString stringWithFormat:@"%@", [station valueForKey:AVAILABLE_DOCKS]];
    }
}

@end
