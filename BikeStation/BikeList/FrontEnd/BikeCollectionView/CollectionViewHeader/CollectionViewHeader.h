//
//  collectionViewHeader.h
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CollectionViewHeader : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;


@end

NS_ASSUME_NONNULL_END
