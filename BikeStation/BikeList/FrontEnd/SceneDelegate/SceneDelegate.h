//
//  SceneDelegate.h
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

