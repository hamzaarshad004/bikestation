//
//  detailTableViewController.m
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import "DetailTableViewController.h"
#import "DetailTableViewCell.h"
#import <CoreData/CoreData.h>
#import "Constants.h"

@interface DetailTableViewController (){
}
@end

@implementation DetailTableViewController
@synthesize tableView;


- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.estimatedRowHeight = 100.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableCell" forIndexPath:indexPath];
    
    [cell setTableCells:self.bike];
    
    return cell;
}

- (IBAction)statusChange:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    if (self.bike){
        [self.bike setValue:STATUS_VALUE_OFF_SERVICE forKey:STATUS_VALUE];
        NSNumber *myNumber = [NSNumber numberWithInt:3];
        [self.bike setValue:myNumber forKey:STATUS_KEY];
    }
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    [self.tableView reloadData];
}

@end
