//
//  detailTableViewController.h
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailTableViewController : UITableViewController

@property (weak, nonatomic) NSString *statusValue;
@property (nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISwitch *statusSwitch;
@property (strong) NSManagedObject *bike;

- (IBAction)statusChange:(id)sender;

@end

NS_ASSUME_NONNULL_END
