//
//  DetailTableViewCell.h
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DetailTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblStatusValue;
@property (strong, nonatomic) IBOutlet UISwitch *swtStatus;

-(void) setTableCells: (NSManagedObject*) stationDetail;

@end

NS_ASSUME_NONNULL_END
