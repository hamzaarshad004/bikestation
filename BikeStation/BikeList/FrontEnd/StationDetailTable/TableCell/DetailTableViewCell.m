//
//  DetailTableViewCell.m
//  BikeList
//
//  Created by ladmin on 28/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import "DetailTableViewCell.h"
#import "Constants.h"

@implementation DetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) setTableCells: (NSManagedObject*) stationDetail{
    if (stationDetail){
        self.lblStatusValue.text = [stationDetail valueForKey:STATUS_VALUE];
    }
}

@end
