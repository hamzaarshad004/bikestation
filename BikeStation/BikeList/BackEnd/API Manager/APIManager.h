//
//  APIManager.h
//  BikeList
//
//  Created by ladmin on 03/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "AFNetworking.h"

NS_ASSUME_NONNULL_BEGIN

@interface APIManager : NSObject{
    NSURLSessionConfiguration *sessionConfiguration;
    NSURLSession *session;
}

+(id) getAPIManager;
-(void) configureSession;
-(void) getBikeStations:(void (^)(NSDictionary *data))completionBlock;

@end

NS_ASSUME_NONNULL_END
