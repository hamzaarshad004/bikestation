//
//  APIManager.m
//  BikeList
//
//  Created by ladmin on 03/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import "APIManager.h"
#import <dispatch/dispatch.h>
#import "Constants.h"

@implementation APIManager

#pragma mark singleton methods

+(id) getAPIManager{
    static APIManager *apiManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        apiManager = [[self alloc] init];
    });
    return apiManager;
}

- (id)init {
  if (self = [super init]) {
  }
  return self;
}


#pragma mark API Calls

-(void) getBikeStations:(void (^)(NSDictionary *data))completionBlock{
    dispatch_queue_t dataQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    dispatch_async(dataQueue, ^(void){
        
        [self configureSession];
        NSURL *url = [NSURL URLWithString: STATION_LIST_API];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:self->sessionConfiguration];
        NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error);
            } else {
                NSDictionary* test = (NSDictionary*) responseObject;
                NSLog(@"%@ %@", response, test);
                if (completionBlock){
                    completionBlock(test);
                }
            }
        }];
        [dataTask resume];
    });
}


#pragma mark  - Session Configuration

-(void) configureSession{
    sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        
       // Configure Session Configuration
    [sessionConfiguration setAllowsCellularAccess:YES];
    [sessionConfiguration setHTTPAdditionalHeaders:@{ ACCEPT_ALL_HEADERS }];
    
    session = [NSURLSession sessionWithConfiguration:self->sessionConfiguration];
}

@end
