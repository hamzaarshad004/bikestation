//
//  CoreDataManager.h
//  BikeList
//
//  Created by ladmin on 03/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CoreDataManager : NSObject

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory; // nice to have to reference files for core data

+ (id) getCoreDataManager;

- (void) populateStationList:(NSArray*) stations;
- (NSMutableArray*) getStationList;
- (void) deleteStation:(NSManagedObject*) station;

@end

NS_ASSUME_NONNULL_END
