//
//  CoreDataManager.m
//  BikeList
//
//  Created by ladmin on 03/06/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import "CoreDataManager.h"
#import "Station.h"
#import "Constants.h"


@interface CoreDataManager(){
   // __block NSManagedObjectContext *context;
}

@end

@implementation CoreDataManager


#pragma mark singleton methods

+(id) getCoreDataManager{
    static CoreDataManager *coreDataManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        coreDataManager = [[self alloc] init];
    });
    return coreDataManager;
}

- (id)init {
  if (self = [super init]) {
  }
  return self;
}

#pragma mark getManagedObjectContext

- (NSManagedObjectContext *)getManagedObjectContext
{
    NSManagedObjectContext *context = nil;
    
//    id delegate = [[UIApplication sharedApplication] delegate];
//    if ([delegate performSelector:@selector(managedObjectContext)]) {
//        context = [delegate managedObjectContext];
//    }
    context = [self managedObjectContext];
    return context;
}

#pragma mark BikeList entity Functions

- (void) populateStationList:(NSArray*) stations{
//    dispatch_async(dispatch_get_main_queue(), ^(void) {
//        self->context = [self managedObjectContext];
//    });
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:BIKE_LIST];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];

    NSError *deleteError = nil;
    [[self getManagedObjectContext] executeRequest:delete error:&deleteError];

    for(int i = 0; i< [stations count]; i++){
        
        Station *item = [NSEntityDescription insertNewObjectForEntityForName:BIKE_LIST inManagedObjectContext: [self getManagedObjectContext]];
        //NSLog(@"HERE COMES %@", [Stations[0] valueForKey:@"id"]);
        
        item.identity = [[stations[i] valueForKey:@"id"] integerValue];
        item.stationName = [stations[i] valueForKey:STATION_NAME];
        item.statusValue = [stations[i] valueForKey:STATUS_VALUE];
        item.statusKey = [stations[i] valueForKey:STATUS_KEY];
        item.availableDocks = [[stations[i] valueForKey:AVAILABLE_DOCKS]integerValue];
        /* ... so on ... */

        NSError *error = nil;
        if ([self getManagedObjectContext] != nil) {
            if ([[self getManagedObjectContext] hasChanges] && ![[self getManagedObjectContext] save:&error]) {
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
        }
    }
}

-(NSMutableArray*) getStationList{
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:BIKE_LIST];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"%K == %@",STATUS_VALUE, STATUS_VALUE_IN_SERVICE];
        return [[[self getManagedObjectContext] executeFetchRequest:fetchRequest error:nil] mutableCopy];
}

-(void) deleteStation:(NSManagedObject*) station{
    
    NSNumber *myNumber = [NSNumber numberWithInt:[[station valueForKey:IDENTITY]intValue]];
    //NSLog(@"It is coming up here %@", myNumber);
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    fetch.entity = [NSEntityDescription entityForName:BIKE_LIST inManagedObjectContext:[self getManagedObjectContext]];
    fetch.predicate = [NSPredicate predicateWithFormat:@"%K == %@",IDENTITY, myNumber];
    NSArray *array = [[self getManagedObjectContext] executeFetchRequest:fetch error:nil];
    
    for (NSManagedObject *managedObject in array) {
        [[self getManagedObjectContext] deleteObject:managedObject];
    }
    NSError* error = nil;
    [[self getManagedObjectContext] save:&error];
}

#pragma mark Core Data Stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)saveContext{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"BikeList" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"BikeList.sqlite"];

    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {

        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _persistentStoreCoordinator;
}


@end
