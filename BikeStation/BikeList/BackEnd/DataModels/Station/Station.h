//
//  Station.h
//  BikeList
//
//  Created by ladmin on 29/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Station : NSManagedObject
    @property (nonatomic) NSInteger identity;
    @property (nonatomic) NSInteger availableDocks;
    @property (nonatomic, retain) NSString * stationName;
    @property (nonatomic, retain) NSNumber * statusKey;
    @property (nonatomic, retain) NSString * statusValue;
@end

NS_ASSUME_NONNULL_END
