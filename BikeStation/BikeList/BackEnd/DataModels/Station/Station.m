//
//  Station.m
//  BikeList
//
//  Created by ladmin on 29/05/2020.
//  Copyright © 2020 hamza. All rights reserved.
//

#import "Station.h"

@implementation Station
@synthesize statusKey;
@synthesize stationName;
@synthesize statusValue;
@synthesize availableDocks;
@synthesize identity;
@end
