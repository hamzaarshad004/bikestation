//
//  BikeList+CoreDataProperties.h
//  
//
//  Created by ladmin on 04/06/2020.
//
//  This file was automatically generated and should not be edited.
//

#import "BikeList+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface BikeList (CoreDataProperties)

+ (NSFetchRequest<BikeList *> *)fetchRequest;

@property (nonatomic) int32_t availableDocks;
@property (nonatomic) int16_t identity;
@property (nullable, nonatomic, copy) NSString *stationName;
@property (nonatomic) int32_t statusKey;
@property (nullable, nonatomic, copy) NSString *statusValue;

@end

NS_ASSUME_NONNULL_END
