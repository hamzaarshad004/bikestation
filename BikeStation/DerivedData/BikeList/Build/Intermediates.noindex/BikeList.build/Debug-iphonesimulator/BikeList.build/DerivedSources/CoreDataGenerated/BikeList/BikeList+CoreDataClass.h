//
//  BikeList+CoreDataClass.h
//  
//
//  Created by ladmin on 04/06/2020.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BikeList : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BikeList+CoreDataProperties.h"
