//
//  BikeList+CoreDataProperties.m
//  
//
//  Created by ladmin on 04/06/2020.
//
//  This file was automatically generated and should not be edited.
//

#import "BikeList+CoreDataProperties.h"

@implementation BikeList (CoreDataProperties)

+ (NSFetchRequest<BikeList *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"BikeList"];
}

@dynamic availableDocks;
@dynamic identity;
@dynamic stationName;
@dynamic statusKey;
@dynamic statusValue;

@end
